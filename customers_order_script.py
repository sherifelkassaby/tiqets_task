
import csv

with open('barcodes.csv') as barcodes_csv:
  	barcodes_array = list(csv.reader(barcodes_csv))

with open('orders.csv') as orders_csv:
  	orders_array = list(csv.reader(orders_csv))


#### validation ###
# duplications validation
uniq_barcodes = []
duplicated_barcodes = []
for barcode_arr in barcodes_array:
	if not any(barcode_arr[0] in x for x in uniq_barcodes):
		uniq_barcodes.append(barcode_arr)
	else:
		duplicated_barcodes.append(barcode_arr[0])


print(str(len(duplicated_barcodes)) + ' Duplicated barcode(s): \n\n' + str(duplicated_barcodes))
print('\n************************************************************************************\n')
# un sold barcodes validation
unsold_barcode = []
sold_barcodes = []
for i,row in enumerate(uniq_barcodes):
	if i > 0 :
		if not row[1].strip():
			unsold_barcode.append(row[0])
		else:
			sold_barcodes.append(row)

print(str(len(unsold_barcode)) + ' Not sold barcode(s): \n\n' + str(unsold_barcode))
print('\n************************************************************************************\n')
### end of validation ###

out_put_arr = []

for i, order_arr in enumerate(orders_array):
	# Drop the Csv Columns header
	if i > 0:
		order_barcodes_arr = []
		for x, barcode_arr in enumerate(sold_barcodes):

			if barcode_arr[1] == order_arr[0]:
				order_barcodes_arr.append(barcode_arr[0])	
	    # print(str(barcode_arr) + ' -> ' + str(order_arr) +out_put_arr)
		if len(order_barcodes_arr) > 0:
			out_put_arr.append([order_arr[1], order_arr[0], order_barcodes_arr, len(order_barcodes_arr)])
		order_barcodes_arr = []

out_put_arr = sorted(out_put_arr, key=lambda a_entry: a_entry[3], reverse = True) 
# print(out_put_arr)

# Top 5 customers
print('Top customers:\n')
for row in out_put_arr[0:5]:
	print('customer id: ' + str(row[0]) + ' bought ' + str(row[3]) + ' tickets.')



with open('output.csv', 'w') as csvfile:
    fieldnames = ['customer_id', 'order_id', 'barcodes']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for row in out_put_arr:
	    writer.writerow({'customer_id': row[0], 'order_id': row[1], 'barcodes': row[2]} )